package com.bytpayment.services.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Model.LocalCurrency;
import com.bytpayment.services.Services.CodeService;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.jwt.JWTHandler;
import com.bytpayment.services.jwt.JWTVerifications;
import com.bytpayment.services.request.body.RequestCodeReadAll;
import com.bytpayment.services.request.response.ResponseMultipleCodes;
import com.bytpayment.services.request.response.ResponseSingleCode;

@CrossOrigin(origins="*", allowedHeaders="*") // on peut faire origins="http://domain.com"
@RestController
@RequestMapping("/code")
public class CodeController {
	@Autowired
	private CodeService codeService;
	
	@PutMapping("/create")
	public ResponseEntity<ResponseSingleCode> create(@RequestHeader HttpHeaders headers, @RequestBody LocalCurrency localCurrency) {
		ResponseEntity<ResponseSingleCode> checkAuthorization = JWTVerifications.singleCodeAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		JWTDecodedUser currentUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		
		if (!currentUser.canCreateCode()) {
			return new ResponseEntity<ResponseSingleCode>(
				new ResponseSingleCode(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		
		ResponseSingleCode responseSingleCode = this.codeService.insertOne(localCurrency, currentUser.getCode());
		return new ResponseEntity<ResponseSingleCode>(
			responseSingleCode,
			responseSingleCode.getHttpStatus()
		);
	}
	
	@GetMapping("/read/{id}")
	public ResponseEntity<ResponseSingleCode> read(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		ResponseEntity<ResponseSingleCode> checkToken = JWTVerifications.singleCodeNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleCode responseSingleCode = this.codeService.readById(id);
		return new ResponseEntity<ResponseSingleCode>(
			responseSingleCode,
			responseSingleCode.getHttpStatus()
		);
	}
	
	@GetMapping("/readByCode/{code}")
	public ResponseEntity<ResponseSingleCode> readByCode(@RequestHeader HttpHeaders headers, @PathVariable("code") String code) {
		ResponseEntity<ResponseSingleCode> checkToken = JWTVerifications.singleCodeNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleCode responseSingleCode = this.codeService.readByCode(code);
		return new ResponseEntity<ResponseSingleCode>(
			responseSingleCode,
			responseSingleCode.getHttpStatus()
		);
	}
	
	@PostMapping("readAll")
	public ResponseEntity<ResponseMultipleCodes> readAll(@RequestHeader HttpHeaders headers, @RequestBody RequestCodeReadAll request) {
		ResponseEntity<ResponseMultipleCodes> checkToken = JWTVerifications.multipleCodesNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseMultipleCodes responseMultipleCodes = this.codeService.readAll(request);
		return new ResponseEntity<ResponseMultipleCodes>(
			responseMultipleCodes,
			responseMultipleCodes.getHttpStatus()
		);
	}
	
	/*@PostMapping("/useCode/{code}")
	public ResponseEntity<ResponseSingleCode> testUse(@RequestHeader HttpHeaders headers, @PathVariable("code") String code) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseSingleCode>(
				new ResponseSingleCode(jwtResult.getMessage(), null),
				HttpStatus.UNAUTHORIZED
			);
		}
		
		CodeServices cs = new CodeServices(this.codeRepository);
		List<Code> initialCode = cs.readByCode(code);
		Code updatedCode = cs.useCode(initialCode.get(0), jwtResult.getCurrentUser().getCode(), new Date());
		return new ResponseEntity<ResponseSingleCode>(
			new ResponseSingleCode("succes", updatedCode),
			HttpStatus.valueOf(419)
		);
	}*/
}
