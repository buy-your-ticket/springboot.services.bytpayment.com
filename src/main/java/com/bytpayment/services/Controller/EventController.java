package com.bytpayment.services.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Model.Event;
import com.bytpayment.services.Services.EventService;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.jwt.JWTHandler;
import com.bytpayment.services.jwt.JWTVerifications;
import com.bytpayment.services.request.body.RequestEventReadAll;
import com.bytpayment.services.request.response.ResponseMessage;
import com.bytpayment.services.request.response.ResponseMultipleEvents;
import com.bytpayment.services.request.response.ResponseSingleEvent;

@CrossOrigin(origins="*", allowedHeaders="*") // on peut faire origins="http://domain.com"
@RestController
@RequestMapping("/event")
public class EventController {
	@Autowired
	private EventService eventService;
	
	@PutMapping("/create")
	public ResponseEntity<ResponseSingleEvent> create(@RequestHeader HttpHeaders headers, @RequestBody Event event) {
		ResponseEntity<ResponseSingleEvent> checkAuthorization = JWTVerifications.singleEventAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		JWTDecodedUser currentUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		
		if (!currentUser.canCreateEvent()) {
			return new ResponseEntity<ResponseSingleEvent>(
				new ResponseSingleEvent(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		ResponseSingleEvent responseSingleEvent = this.eventService.insertOne(event, currentUser.getCode());
		return new ResponseEntity<ResponseSingleEvent>(
			responseSingleEvent,
			responseSingleEvent.getStatus()
		);
	}
	
	@GetMapping("/read/{id}")
	public ResponseEntity<ResponseSingleEvent> read(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		ResponseEntity<ResponseSingleEvent> checkToken = JWTVerifications.singleEventNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleEvent responseSingleEvent = this.eventService.readById(id);
		return new ResponseEntity<ResponseSingleEvent>(
			responseSingleEvent,
			responseSingleEvent.getStatus()
		);
	}
	
	@GetMapping("/readByCode/{code}")
	public ResponseEntity<ResponseSingleEvent> readByCode(@RequestHeader HttpHeaders headers, @PathVariable("code") String code) {
		ResponseEntity<ResponseSingleEvent> checkToken = JWTVerifications.singleEventNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleEvent responseSingleEvent = this.eventService.readByCode(code);
		return new ResponseEntity<ResponseSingleEvent>(
			responseSingleEvent,
			responseSingleEvent.getStatus()
		);
	}
	
	// Lecture d'un événement par son code, sans retourner dans la 
	// réponse des informations lourdes comme la *photo*
	@GetMapping("/readByCodeSimple/{code}")
	public ResponseEntity<ResponseSingleEvent> readByCodeSimple(@RequestHeader HttpHeaders headers, @PathVariable("code") String code) {
		ResponseEntity<ResponseSingleEvent> checkToken = JWTVerifications.singleEventNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleEvent responseSingleEvent = this.eventService.readByCodeSimple(code);
		return new ResponseEntity<ResponseSingleEvent>(
			responseSingleEvent,
			responseSingleEvent.getStatus()
		);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<ResponseMessage> delete(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		ResponseEntity<ResponseMessage> checkAuthorization = JWTVerifications.messageResponseAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		JWTDecodedUser currentUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		if (!currentUser.canUpdateEvent()) {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage(MessagesREST.FORBIDDEN_ERROR, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		ResponseMessage responseMessage = this.eventService.deleteById(id);
		return new ResponseEntity<ResponseMessage>(
			responseMessage,
			responseMessage.getStatus()
		);
	}
	
	@PostMapping("/update")
	public ResponseEntity<ResponseSingleEvent> update(@RequestHeader HttpHeaders headers, @RequestBody Event event) {
		ResponseEntity<ResponseSingleEvent> checkAuthorization = JWTVerifications.singleEventAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		JWTDecodedUser currentUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		/*if (!currentUser.canUpdateEvent()) {
			return new ResponseEntity<ResponseSingleEvent>(
				new ResponseSingleEvent(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}*/
		
		ResponseSingleEvent responseSingleEvent = this.eventService.update(event, currentUser);
		return new ResponseEntity<ResponseSingleEvent>(
			responseSingleEvent,
			responseSingleEvent.getStatus()
		);
	}
	
	@PostMapping("/readAll")
	public ResponseEntity<ResponseMultipleEvents> readAll(@RequestHeader HttpHeaders headers, @RequestBody RequestEventReadAll request) {
		ResponseEntity<ResponseMultipleEvents> checkToken = JWTVerifications.multipleEventsNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseMultipleEvents responseMultipleEvents = this.eventService.readAll(request);
		return new ResponseEntity<ResponseMultipleEvents>(
			responseMultipleEvents,
			responseMultipleEvents.getStatus()
		);
	}
}
