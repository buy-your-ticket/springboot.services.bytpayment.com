package com.bytpayment.services.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Helpers.TicketsPerPeriode;
import com.bytpayment.services.Services.TicketService;
import com.bytpayment.services.jwt.JWTHandler;
import com.bytpayment.services.jwt.JWTVerifications;
import com.bytpayment.services.request.body.RequestCreateTicketByCode;
import com.bytpayment.services.request.body.RequestTicketReadAll;
import com.bytpayment.services.request.response.ResponseMultipleTickets;
import com.bytpayment.services.request.response.ResponseSingleTicket;

@CrossOrigin(origins="*", allowedHeaders="*") // on peut faire origins="http://domain.com"
@RestController
@RequestMapping("/ticket")
public class TicketController {
	@Autowired
	private TicketService ticketService;
	
	@PutMapping("/createByCode")
	public ResponseEntity<ResponseSingleTicket> createByCode(@RequestHeader HttpHeaders headers, @RequestBody RequestCreateTicketByCode request){
		ResponseEntity<ResponseSingleTicket> checkAuthorization = JWTVerifications.singleTicketAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		JWTDecodedUser currentUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		ResponseSingleTicket responseSingleTicket = this.ticketService.createByCode(request, currentUser.getCode());
		return new ResponseEntity<ResponseSingleTicket>(
			responseSingleTicket,
			responseSingleTicket.getHttpStatus()
		);
	}
	
	@GetMapping("/read/{id}")
	public ResponseEntity<ResponseSingleTicket> read(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		ResponseEntity<ResponseSingleTicket> checkToken = JWTVerifications.singleTicketNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseSingleTicket responseSingleTicket = this.ticketService.readById(id);
		return new ResponseEntity<ResponseSingleTicket>(
			responseSingleTicket,
			responseSingleTicket.getHttpStatus()
		);
	}
	
	@PostMapping("/readAll")
	public ResponseEntity<ResponseMultipleTickets> readAll(@RequestHeader HttpHeaders headers, @RequestBody RequestTicketReadAll request) {
		ResponseEntity<ResponseMultipleTickets> checkToken = JWTVerifications.multipleTicketsNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		ResponseMultipleTickets responseMultipleTickets = this.ticketService.readAll(request);
		return new ResponseEntity<ResponseMultipleTickets>(
			responseMultipleTickets,
			responseMultipleTickets.getHttpStatus()
		);
	}
	
	@GetMapping("/validation/{ticketNumber}")
	public ResponseEntity<ResponseSingleTicket> validation(@RequestHeader HttpHeaders headers, @PathVariable("ticketNumber") String ticketNumber) {
		ResponseEntity<ResponseSingleTicket> checkAuthorization = JWTVerifications.singleTicketAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		JWTDecodedUser decodedUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		ResponseSingleTicket responseSingleTicket = this.ticketService.validation(ticketNumber, decodedUser);
		return new ResponseEntity<ResponseSingleTicket>(
			responseSingleTicket,
			responseSingleTicket.getHttpStatus()
		);
		
	}
	
	@GetMapping("/pause/{ticketNumber}")
	public ResponseEntity<ResponseSingleTicket> pause(@RequestHeader HttpHeaders headers, @PathVariable("ticketNumber") String ticketNumber) {
		ResponseEntity<ResponseSingleTicket> checkAuthorization = JWTVerifications.singleTicketAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		JWTDecodedUser decodedUser = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION)).getCurrentUser();
		ResponseSingleTicket responseSingleTicket = this.ticketService.pause(ticketNumber, decodedUser);
		return new ResponseEntity<ResponseSingleTicket>(
			responseSingleTicket,
			responseSingleTicket.getHttpStatus()
		);
	}
	
	// pour l'instant on cherche par période d'une semaine
	@GetMapping("/ticketsPerWeek/{eventCode}")
	public ResponseEntity<List<TicketsPerPeriode>> ticketsPerWeek(@RequestHeader HttpHeaders headers, @PathVariable("eventCode") String eventCode) {
		ResponseEntity<List<TicketsPerPeriode>> checkToken = JWTVerifications.listTicketsPerPeriodeNoAuthorizationRequired(headers);
		if (checkToken != null) {
			return checkToken;
		}
		List<TicketsPerPeriode> listTicketsPerPeriode = this.ticketService.ticketsPerWeek(eventCode);
		return new ResponseEntity<List<TicketsPerPeriode>>(
			listTicketsPerPeriode,
			HttpStatus.OK
		);
	}
}
