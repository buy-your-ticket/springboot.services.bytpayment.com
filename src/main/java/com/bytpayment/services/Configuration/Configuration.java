package com.bytpayment.services.Configuration;

import java.util.ArrayList;
import java.util.Arrays;

import com.auth0.jwt.algorithms.Algorithm;
import com.bytpayment.services.enums.Role;

public class Configuration {
	// https://github.com/auth0/java-jwt
	public static final String secret = "somesecretmessage";
	public static final Algorithm jwtAlgorithm = Algorithm.HMAC256(secret);
	
	// Requête readAll + Pagination
	public static final int defaultLimit = 50;
	public static final String defaultSortBy = "name";
	public static final String defaultCodeSortBy = "code";
	public static final String defaultTicketSortBy = "createdAt";
	
	// Roles autorisés à créer des événements
	public static final ArrayList<Role> EVENT_CREATION_ROLES =
		new ArrayList<Role>(
			Arrays.asList(
				Role.ADMIN,
				Role.TECH,
				Role.SELLER
			)
		);
	
	// Roles autorisés à modifier des événements
	public static final ArrayList<Role> EVENT_UPDATE_ROLES =
		new ArrayList<Role>(
			Arrays.asList(
				Role.ADMIN,
				Role.TECH
			)
		);
	
	// Roles autorisés à créer un code
	public static final ArrayList<Role> CODE_CREATION_ROLES =
		new ArrayList<Role>(
			Arrays.asList(
				Role.ADMIN,
				Role.TECH
			)
		);
	
	// Roles autorisés à valider les tickets (autre que créateur de l'événement)
	public static final ArrayList<Role> TICKET_VALIDATION_ROLES =
		new ArrayList<Role>(
			Arrays.asList(
				Role.ADMIN,
				Role.TECH
			)
		);
			
}
