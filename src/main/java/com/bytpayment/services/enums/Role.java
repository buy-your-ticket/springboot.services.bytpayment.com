package com.bytpayment.services.enums;

public enum Role {
	USER ("USER"),
	SELLER ("SELLER"),
	TECH ("TECH"),
	ADMIN ("ADMIN");
	
	private String role = "";
	
	Role(String role) {
		this.role = role;
	}
	
	public String toString() {
		return this.role;
	}
}
