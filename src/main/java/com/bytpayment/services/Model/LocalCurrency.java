package com.bytpayment.services.Model;

import com.bytpayment.services.Helpers.Helper;
import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
public class LocalCurrency {
	private Currency currency;
	private float value;
	
	public LocalCurrency(Currency currency, float value) {
		this.currency = currency;
		this.value = value;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}
	
	public Boolean validity() { // value par défaut sera 0
		return !Helper.strEmpty(this.currency.toString());
	}
	
	public Boolean equals(LocalCurrency localCurrency) {
		return (
			this.currency.equals(localCurrency.getCurrency()) &&
			this.value == localCurrency.getValue()
		);
	}
}
