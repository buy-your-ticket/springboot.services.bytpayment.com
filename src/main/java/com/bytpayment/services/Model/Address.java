package com.bytpayment.services.Model;

import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
public class Address {
	private String country;
	private String city;
	private String road;
	private String number;
	private String details;
	
	public Address(String country, String city, String road, String number, String details) {
		this.country = country;
		this.city = city;
		this.road = road;
		this.number = number;
		this.details = details;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
