package com.bytpayment.services.Model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
@Document(collection = "Codes")
public class Code {
	@Id
	private String id;
	private String code;
	private LocalCurrency value;
	private String createdBy;
	private Date createdAt;
	private String usedBy;
	private Date usedAt;
	
	public Code(String id, String code, LocalCurrency value, String createdBy, Date createdAt, String usedBy,
			Date usedAt) {
		this.id = id;
		this.code = code;
		this.value = value;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.usedBy = usedBy;
		this.usedAt = usedAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalCurrency getValue() {
		return value;
	}

	public void setValue(LocalCurrency value) {
		this.value = value;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public Date getUsedAt() {
		return usedAt;
	}

	public void setUsedAt(Date usedAt) {
		this.usedAt = usedAt;
	}	
}
