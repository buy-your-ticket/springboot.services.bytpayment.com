package com.bytpayment.services.Model;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.bytpayment.services.Helpers.Helper;
import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
@Document(collection = "Events")
public class Event {
	@Id
	private String id;
	private String code;
	private String name;
	private String description;
	private ArrayList<String> tags;
	private Date date;
	private Address venue;
	private ArrayList<Price> prices;
	private String seller;
	private String photo;
	private int lockRevision;
	
	public Event(String id, String code, String name, String description, ArrayList<String> tags, Date date,
			Address venue, ArrayList<Price> prices, String seller, String photo, int lockRevision) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.tags = tags;
		this.date = date;
		this.venue = venue;
		this.prices = prices;
		this.seller = seller;
		this.photo = photo;
		this.lockRevision = lockRevision;
	}	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Address getVenue() {
		return venue;
	}

	public void setVenue(Address venue) {
		this.venue = venue;
	}

	public ArrayList<Price> getPrices() {
		return prices;
	}

	public void setPrices(ArrayList<Price> prices) {
		this.prices = prices;
	}
	
	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getLockRevision() {
		return lockRevision;
	}

	public void setLockRevision(int lockRevision) {
		this.lockRevision = lockRevision;
	}

	public Boolean pricesValidity() {
		if (this.prices == null) {
			return false;
		}
		for (Price price : this.prices) {
			if (!price.validity()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Méthode qui permet de tester si un événement est valide
	 * avant son insertion dans la base de données (les attributs à tester proviennent du corps de la requête)
	 * Pour qu'un événement soit valide à la création, il faut qu'il ait au moins :
	 * - name
	 * - date
	 * - prices (vérifier aussi la validité des prix) 
	 * @return boolean
	 */
	public Boolean validity() {
		return (
			!Helper.strEmpty(this.name) &&
			(this.date != null) &&
			this.pricesValidity()
		);
	}
	
	public Price priceById(String id) {
		for (Price p : prices) {
			if (p.getId().equals(id)) {
				return p;
			}
		}
		return null;
	}
	
	/*public int indexPriceByType(String type) {
		for (int i = 0; i < this.prices.size(); i++) {
			if (this.prices.get(i).getType().equalsIgnoreCase(type)) {
				return i;
			}
		}
		return -1;
	}*/
	
	//public int getIndexPrice(String type)
}
