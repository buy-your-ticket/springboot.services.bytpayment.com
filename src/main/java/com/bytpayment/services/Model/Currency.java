package com.bytpayment.services.Model;

public enum Currency {
	BIF("BIF"),
	USD ("USD");
	
	private String currency = "";
	
	Currency(String currency) {
		this.currency = currency;
	}
	
	public String toString() {
		return this.currency;
	}
}
