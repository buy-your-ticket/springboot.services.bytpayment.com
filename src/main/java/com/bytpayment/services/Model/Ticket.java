package com.bytpayment.services.Model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
@Document(collection = "Tickets")
public class Ticket {
	@Id
	private String id;
	private String ticketNumber;
	private String code;
	private String eventCode;
	private String seller;
	private String user;
	private String priceId;
	private LocalCurrency price;
	private Date createdAt;
	private Boolean valide;
	private Date valideStatusChangedAt;
	
	public Ticket(String id, String ticketNumber, String code, String eventCode, String seller, String user,
			String priceId, LocalCurrency price, Date createdAt, Boolean valide, Date valideStatusChangedAt) {
		this.id = id;
		this.ticketNumber = ticketNumber;
		this.code = code;
		this.eventCode = eventCode;
		this.seller = seller;
		this.user = user;
		this.priceId = priceId;
		this.price = price;
		this.createdAt = createdAt;
		this.valide = valide;
		this.valideStatusChangedAt = valideStatusChangedAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPriceId() {
		return priceId;
	}

	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}

	public LocalCurrency getPrice() {
		return price;
	}

	public void setPrice(LocalCurrency price) {
		this.price = price;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getValide() {
		return valide;
	}

	public void setValide(Boolean valide) {
		this.valide = valide;
	}

	public Date getValideStatusChangedAt() {
		return valideStatusChangedAt;
	}

	public void setValideStatusChangedAt(Date valideStatusChangedAt) {
		this.valideStatusChangedAt = valideStatusChangedAt;
	}
}
