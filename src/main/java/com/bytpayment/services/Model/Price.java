package com.bytpayment.services.Model;

import java.util.ArrayList;

import com.bytpayment.services.Helpers.Helper;
import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
public class Price {
	private String id;
	private String type;
	private ArrayList<LocalCurrency> values;
	private int totalPlaces;
	private int alreadyTakenPlaces;
	private Boolean checkExceeding;
	
	public Price(String id, String type, ArrayList<LocalCurrency> values, int totalPlaces, int alreadyTakenPlaces,
			Boolean checkExceeding) {
		this.id = id;
		this.type = type;
		this.values = values;
		this.totalPlaces = totalPlaces;
		this.alreadyTakenPlaces = alreadyTakenPlaces;
		this.checkExceeding = checkExceeding;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<LocalCurrency> getValues() {
		return values;
	}

	public void setValues(ArrayList<LocalCurrency> values) {
		this.values = values;
	}

	public int getTotalPlaces() {
		return totalPlaces;
	}

	public void setTotalPlaces(int totalPlaces) {
		this.totalPlaces = totalPlaces;
	}

	public int getAlreadyTakenPlaces() {
		return alreadyTakenPlaces;
	}

	public void setAlreadyTakenPlaces(int alreadyTakenPlaces) {
		this.alreadyTakenPlaces = alreadyTakenPlaces;
	}

	public Boolean getCheckExceeding() {
		return checkExceeding;
	}

	public void setCheckExceeding(Boolean checkExceeding) {
		this.checkExceeding = checkExceeding;
	}

	public Boolean valuesValidity() {
		if (this.values == null) {
			return false;
		}
		for (LocalCurrency localCurrency : this.values) {
			if (!localCurrency.validity()) {
				return false;
			}
		}
		return true;
	}
	
	public Boolean placesValidity() {
		if (this.checkExceeding == null) {
			return true;
		}
		if (this.checkExceeding) {
			return (this.totalPlaces >= this.alreadyTakenPlaces);
		}
		return true;
	}
	
	public Boolean validity() {
		return (
			!Helper.strEmpty(this.type) &&
			this.valuesValidity() &&
			this.placesValidity()
		);
	}
	
	public Boolean containsLocalCurrency(LocalCurrency localCurrency) {
		for (LocalCurrency l : this.values) {
			if (l.equals(localCurrency)) {
				return true;
			}
		}
		return false;
	}
}
