package com.bytpayment.services.constantes;

public class MessagesREST {
	public static final String BAD_REQUEST_ERROR = "Paramètres de la requête non valides";
	public static final String DELETE_SUCCESS = "Suppression de l'enregistrement effectuée avec succès";
	public static final String FORBIDDEN_ERROR = "Vous ne disposez pas de droits suffisants pour effectuer cette opération";
	public static final String SEARCH_RESULT = "Résultat de la recherche";
	
	public static final String CODE_ALREADY_USED = "Code déjà utilisé";
	public static final String CODE_CREATION_SUCCESS = "Code créé";
	public static final String CODE_NOT_FOUND = "Aucun code trouvé";
	public static final String CODE_READ_SUCCESS = "Code(s) trouvé(s)";
	public static final String CODE_UNABLE_CREATE_TICKET = "La valeur du ticket a créer doit correspondre à la valeur du code";
	
	public static final String EVENT_CREATION_SUCCESS = "Événement créé";
	public static final String EVENT_NOT_FOUND = "Aucun événement trouvé";
	public static final String EVENT_READ_SUCCESS = "Évenement(s) trouvé(s)";
	public static final String EVENT_UPDATE_SUCCESS = "Mise à jour de l'événement effectuée avec succès";
	
	public static final String PRICE_NOT_FOUND = "Aucun prix n'a été trouvé";
	
	public static final String TICKET_CREATION_ERROR = "Ticket non créé";
	public static final String TICKET_CREATION_SUCCESS = "Ticket créé";
	public static final String TICKET_NOT_FOUND = "Aucun ticket trouvé";
	public static final String TICKET_PAUSE_ERROR = "L'opération de mise en pause du ticket a échoué";
	public static final String TICKET_PAUSE_SUCCESS = "Ticket mis en pause";
	public static final String TICKET_READ_SUCCESS = "Ticket(s) trouvé(s)";
	public static final String TICKET_VALIDATED = "Ticket déjà validé";
	public static final String TICKET_VALIDATION_SUCCESS = "Validation du ticket avec succès";
}
