package com.bytpayment.services.request.response;

import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Code;

public class ResponseSingleCode {
	private String message;
	private Code code;
	private HttpStatus httpStatus;
	
	public ResponseSingleCode(String message, Code code, HttpStatus httpStatus) {
		this.message = message;
		this.code = code;
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
