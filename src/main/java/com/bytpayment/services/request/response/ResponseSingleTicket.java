package com.bytpayment.services.request.response;

import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Ticket;

public class ResponseSingleTicket {
	private String message;
	private Ticket ticket;
	private HttpStatus httpStatus;
	
	public ResponseSingleTicket(String message, Ticket ticket, HttpStatus httpStatus) {
		this.message = message;
		this.ticket = ticket;
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
