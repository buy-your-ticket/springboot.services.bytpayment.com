package com.bytpayment.services.request.response;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Event;

public class ResponseMultipleEvents {
	private String message;
	private Page<Event> results;
	private HttpStatus status;
	
	public ResponseMultipleEvents(String message, Page<Event> results, HttpStatus status) {
		this.message = message;
		this.results = results;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Page<Event> getResults() {
		return results;
	}

	public void setResults(Page<Event> results) {
		this.results = results;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
