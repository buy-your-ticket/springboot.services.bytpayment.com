package com.bytpayment.services.request.response;

import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Event;

public class ResponseSingleEvent {
	private String message;
	private Event event;
	private HttpStatus status;
	
	public ResponseSingleEvent(String message, Event event, HttpStatus status) {
		this.message = message;
		this.event = event;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
