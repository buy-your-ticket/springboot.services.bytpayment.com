package com.bytpayment.services.request.response;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Ticket;

public class ResponseMultipleTickets {
	private String message;
	private Page<Ticket> results;
	private HttpStatus httpStatus;
	
	public ResponseMultipleTickets(String message, Page<Ticket> results, HttpStatus httpStatus) {
		this.message = message;
		this.results = results;
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Page<Ticket> getResults() {
		return results;
	}

	public void setResults(Page<Ticket> results) {
		this.results = results;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
