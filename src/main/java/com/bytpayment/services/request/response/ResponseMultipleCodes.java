package com.bytpayment.services.request.response;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import com.bytpayment.services.Model.Code;

public class ResponseMultipleCodes {
	private String message;
	private Page<Code> results;
	private HttpStatus httpStatus;
	
	public ResponseMultipleCodes(String message, Page<Code> results, HttpStatus httpStatus) {
		this.message = message;
		this.results = results;
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Page<Code> getResults() {
		return results;
	}

	public void setResults(Page<Code> results) {
		this.results = results;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
