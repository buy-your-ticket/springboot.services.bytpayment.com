package com.bytpayment.services.request.body;

import com.bytpayment.services.Helpers.Helper;

public class RequestCreateTicketByCode {
	private String code;
	private String priceId;
	private String eventCode;
	
	public RequestCreateTicketByCode(String code, String priceId, String eventCode) {
		this.code = code;
		this.priceId = priceId;
		this.eventCode = eventCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPriceId() {
		return priceId;
	}

	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public Boolean validity() {
		return (
			!Helper.strEmpty(this.code) &&
			!Helper.strEmpty(this.priceId) &&
			!Helper.strEmpty(this.eventCode)
		);
	}
}
