package com.bytpayment.services.request.body;

import java.util.ArrayList;
import java.util.Date;

import com.bytpayment.services.Model.Address;

public class RequestEventReadAll {
	private String name;
	private String tag;
	private Date date;
	private Address venue;
	private String seller;
	private int limit;
	private int offset;
	private String sortBy;
	private ArrayList<String> exfields; // champs à ne pas retourner
	
	public RequestEventReadAll(
			String name, String tag, Date date, Address venue, String seller, int limit, int offset, String sortBy, ArrayList<String> exfields) {
		this.name = name;
		this.tag = tag;
		this.date = date;
		this.venue = venue;
		this.seller = seller;
		this.limit = limit;
		this.offset = offset;
		this.sortBy = sortBy;
		this.exfields = exfields;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Address getVenue() {
		return venue;
	}

	public void setVenue(Address venue) {
		this.venue = venue;
	}
	
	public String getSeller() {
		return seller;
	}
	
	public void setSeller(String seller) {
		this.seller = seller;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public ArrayList<String> getExfields() {
		return exfields;
	}

	public void setExfields(ArrayList<String> exfields) {
		this.exfields = exfields;
	}	
}
