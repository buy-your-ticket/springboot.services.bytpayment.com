package com.bytpayment.services.request.body;

import java.util.Date;

import com.bytpayment.services.Model.LocalCurrency;

public class RequestTicketReadAll {
	private String eventCode;
	private String seller;
	private String user;
	private LocalCurrency price;
	private Date createdAt;
	private Boolean valide;
	private Date valideStatusChangedAt;
	private int limit;
	private int offset;
	private String sortBy;
	
	public RequestTicketReadAll(String eventCode, String seller, String user, LocalCurrency price, Date createdAt,
			Boolean valide, Date valideStatusChangedAt, int limit, int offset, String sortBy) {
		this.eventCode = eventCode;
		this.seller = seller;
		this.user = user;
		this.price = price;
		this.createdAt = createdAt;
		this.valide = valide;
		this.valideStatusChangedAt = valideStatusChangedAt;
		this.limit = limit;
		this.offset = offset;
		this.sortBy = sortBy;
	}

	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public LocalCurrency getPrice() {
		return price;
	}

	public void setPrice(LocalCurrency price) {
		this.price = price;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getValide() {
		return valide;
	}

	public void setValide(Boolean valide) {
		this.valide = valide;
	}

	public Date getValideStatusChangedAt() {
		return valideStatusChangedAt;
	}

	public void setValideStatusChangedAt(Date valideStatusChangedAt) {
		this.valideStatusChangedAt = valideStatusChangedAt;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}	
}
