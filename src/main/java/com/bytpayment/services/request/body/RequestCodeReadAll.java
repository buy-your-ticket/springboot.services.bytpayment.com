package com.bytpayment.services.request.body;

import java.util.ArrayList;
import java.util.Date;

import com.bytpayment.services.Model.LocalCurrency;

public class RequestCodeReadAll {
	private LocalCurrency value;
	private String createdBy;
	private Date createdAt;
	private String usedBy;
	private Date usedAt;
	private int limit;
	private int offset;
	private String sortBy;
	private ArrayList<String> exfields;
	
	public RequestCodeReadAll(LocalCurrency value, String createdBy, Date createdAt, String usedBy, Date usedAt,
			int limit, int offset, String sortBy, ArrayList<String> exfields) {
		this.value = value;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.usedBy = usedBy;
		this.usedAt = usedAt;
		this.limit = limit;
		this.offset = offset;
		this.sortBy = sortBy;
		this.exfields = exfields;
	}

	public LocalCurrency getValue() {
		return value;
	}

	public void setValue(LocalCurrency value) {
		this.value = value;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public Date getUsedAt() {
		return usedAt;
	}

	public void setUsedAt(Date usedAt) {
		this.usedAt = usedAt;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public ArrayList<String> getExfields() {
		return exfields;
	}

	public void setExfields(ArrayList<String> exfields) {
		this.exfields = exfields;
	}
}
