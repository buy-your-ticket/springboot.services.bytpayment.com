package com.bytpayment.services.Services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bytpayment.services.Configuration.Configuration;
import com.bytpayment.services.Helpers.Helper;
import com.bytpayment.services.Model.Code;
import com.bytpayment.services.Model.LocalCurrency;
import com.bytpayment.services.Model.QCode;
import com.bytpayment.services.Repository.CodeRepository;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.request.body.RequestCodeReadAll;
import com.bytpayment.services.request.response.ResponseMultipleCodes;
import com.bytpayment.services.request.response.ResponseSingleCode;
import com.querydsl.core.BooleanBuilder;

@Service
public class CodeService {
	@Autowired
	private CodeRepository codeRepository;
	
	public ResponseSingleCode insertOne(LocalCurrency localCurrency, String userCode) {
		if (!localCurrency.validity()) {
			return new ResponseSingleCode(MessagesREST.BAD_REQUEST_ERROR, null, HttpStatus.BAD_REQUEST);
		}
		
		// génération d'un code unique
		String generatedCode = "";
		do {
			generatedCode = Helper.codeGeneration(10, 20);
		} while(!this.codeRepository.findByCode(generatedCode).isEmpty());
		
		Code codeToSave = new Code(
			null,
			generatedCode,
			localCurrency,
			userCode,
			new Date(),
			null,
			null
		);
		
		Code insertedCode = this.codeRepository.insert(codeToSave);
		
		return new ResponseSingleCode(MessagesREST.CODE_CREATION_SUCCESS, insertedCode, HttpStatus.OK);
	}
	
	public ResponseMultipleCodes readAll(RequestCodeReadAll request) {
		QCode qCode = new QCode("code");
		BooleanBuilder predicate = new BooleanBuilder();
		
		if (request.getCreatedBy() != null) {
			predicate.and(qCode.createdBy.eq(request.getCreatedBy()));
		}
		if (request.getUsedBy() != null) {
			predicate.and(qCode.usedBy.eq(request.getUsedBy()));
		}
		if (request.getCreatedAt() != null) {
			predicate.and(qCode.createdAt.goe(request.getCreatedAt()));
		}
		if (request.getUsedAt() != null) {
			predicate.and(qCode.usedAt.goe(request.getUsedAt()));
		}
		// TODO : il faut vérifier qu'on peut filter sur nature des devises seulement
		if (request.getValue() != null) {
			if (request.getValue().getCurrency() != null) {
				predicate.and(qCode.value.currency.eq(request.getValue().getCurrency()));
			}
			predicate.and(qCode.value.value.goe(request.getValue().getValue()));
		}
		
		int limit = 
			request.getLimit() < 1 ? Configuration.defaultLimit : request.getLimit();
		int offset = request.getOffset();
		String sortBy = request.getSortBy() == null ? Configuration.defaultCodeSortBy : request.getSortBy();
		
		Page<Code> results = (Page<Code>) this.codeRepository.findAll(
			predicate,
			PageRequest.of(offset, limit, Sort.by(sortBy))
		);
		
		// extraction des champs
		ArrayList<String> exfields = request.getExfields();
		if (!Helper.arrayListEmpty(exfields)) {
			for (int i = 0; i < results.getContent().size(); i++) {
				if (exfields.contains("id")) {
					results.getContent().get(i).setId(null);
				}
				if (exfields.contains("code")) {
					results.getContent().get(i).setCode(null);
				}
				if (exfields.contains("value")) {
					results.getContent().get(i).setValue(null);
				}
				if (exfields.contains("createdBy")) {
					results.getContent().get(i).setCreatedBy(null);
				}
				if (exfields.contains("createdAt")) {
					results.getContent().get(i).setCreatedAt(null);
				}
				if (exfields.contains("usedBy")) {
					results.getContent().get(i).setUsedBy(null);
				}
				if (exfields.contains("usedAt")) {
					results.getContent().get(i).setUsedAt(null);
				}
			}
		}
		
		return new ResponseMultipleCodes(MessagesREST.SEARCH_RESULT, results, HttpStatus.OK);
	}
	
	public ResponseSingleCode readByCode(String code) {
		List<Code> codes = this.codeRepository.findByCode(code);
		if (codes.isEmpty()) {
			return new ResponseSingleCode(MessagesREST.CODE_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}		
		return new ResponseSingleCode(MessagesREST.CODE_READ_SUCCESS, codes.get(0), HttpStatus.OK);
	}	
	
	public ResponseSingleCode readById(String id) {
		Optional<Code> code = this.codeRepository.findById(id);
		if (!code.isPresent()) {
			return new ResponseSingleCode(MessagesREST.CODE_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		return new ResponseSingleCode(MessagesREST.CODE_READ_SUCCESS, code.get(), HttpStatus.OK);
	}
	
	public Code useCode(Code code, String user, Date date) {
		code.setUsedBy(user);
		code.setUsedAt(date);
		return this.codeRepository.save(code);
	}
}
