package com.bytpayment.services.Services;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bytpayment.services.Configuration.Configuration;
import com.bytpayment.services.Helpers.Helper;
import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Model.Event;
import com.bytpayment.services.Model.Price;
import com.bytpayment.services.Model.QEvent;
import com.bytpayment.services.Repository.EventRepository;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.request.body.RequestEventReadAll;
import com.bytpayment.services.request.response.ResponseMessage;
import com.bytpayment.services.request.response.ResponseMultipleEvents;
import com.bytpayment.services.request.response.ResponseSingleEvent;
import com.querydsl.core.BooleanBuilder;

@Service
public class EventService {
	@Autowired
	private EventRepository eventRepository;
	
	public ResponseMessage deleteById(String id) {
		Optional<Event> event = this.eventRepository.findById(id);
		if (!event.isPresent()) {
			return new ResponseMessage(MessagesREST.EVENT_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
		this.eventRepository.deleteById(id);
		return new ResponseMessage(MessagesREST.DELETE_SUCCESS, HttpStatus.OK);
	}
	
	public ResponseSingleEvent insertOne(Event event, String userCode) {
		if (!event.validity()) {
			return new ResponseSingleEvent(MessagesREST.BAD_REQUEST_ERROR, null, HttpStatus.BAD_REQUEST);
		}
		// prix
		ArrayList<Price> prices = event.getPrices();
		for (Price p : prices) {
			p.setId(Helper.getUniqueId());
		}
		
		// génération d'un code unique
		String generatedCode = "";
		do {
			generatedCode = Helper.codeGeneration(5, 20);
		} while(!this.eventRepository.findByCode(generatedCode).isEmpty());
		event.setCode(generatedCode);
		
		// remplissage du champ seller
		event.setSeller(userCode);
		
		// tags en lower case
		if (event.getTags() != null) {
			ArrayList<String> lowerCaseTags = new ArrayList<String>();
			for (int i = 0; i < event.getTags().size(); i++) {
				lowerCaseTags.add(event.getTags().get(i).toLowerCase());
			}
			event.setTags(lowerCaseTags);
		}
		
		// lockRevision à 0
		if (event.getLockRevision() != 0) {
			event.setLockRevision(0);
		}
				
		// insertion dans la base de données
		Event insertedEvent = this.eventRepository.insert(event);
		return new ResponseSingleEvent(MessagesREST.EVENT_CREATION_SUCCESS, insertedEvent, HttpStatus.OK);
	}
	
	public ResponseMultipleEvents readAll(RequestEventReadAll request) {
		QEvent qEvent = new QEvent("event");
		BooleanBuilder predicate = new BooleanBuilder();
		
		if (request.getName() != null) {
			predicate.and(qEvent.name.startsWithIgnoreCase(request.getName()));
		}
		if (request.getTag() != null) {
				predicate.and(qEvent.tags.contains(request.getTag().toLowerCase()));
		}
		if (request.getDate() != null) {
			predicate.and(qEvent.date.goe(request.getDate()));
		}
		// filtre sur l'adresse
		if (request.getVenue() != null) {
			if (request.getVenue().getCity() != null) {
				predicate.and(qEvent.venue.city.equalsIgnoreCase(request.getVenue().getCity()));
			}
			if (request.getVenue().getCountry() != null) {
				predicate.and(qEvent.venue.country.equalsIgnoreCase(request.getVenue().getCountry()));
			}
			if (request.getVenue().getNumber() != null) {
				predicate.and(qEvent.venue.number.startsWithIgnoreCase(request.getVenue().getNumber()));
			}
			if (request.getVenue().getRoad() != null) {
				predicate.and(qEvent.venue.road.startsWithIgnoreCase(request.getVenue().getRoad()));
			}
			if (request.getVenue().getDetails() != null) {
				predicate.and(qEvent.venue.details.contains(request.getVenue().getDetails()));
			}
		}
		if (request.getSeller() != null) {
			predicate.and(qEvent.seller.eq(request.getSeller()));
		}
		
		int limit = 
			request.getLimit() < 1 ? Configuration.defaultLimit : request.getLimit();
		int offset = request.getOffset(); // au lieu de null, cette valeur vaut 0
		String sortBy = request.getSortBy() == null ? Configuration.defaultSortBy : request.getSortBy();
		Page<Event> results = (Page<Event>) this.eventRepository.findAll(
			predicate,
			PageRequest.of(offset, limit, Sort.by(sortBy))
		);
		
		// extraction des champs
		ArrayList<String> exfields = request.getExfields();
		if (!Helper.arrayListEmpty(exfields)) {
			for (int i = 0; i < results.getContent().size(); i++) {
				if (exfields.contains("id")) {
					results.getContent().get(i).setId(null);
				}
				if (exfields.contains("code")) {
					results.getContent().get(i).setCode(null);
				}
				if (exfields.contains("name")) {
					results.getContent().get(i).setName(null);
				}
				if (exfields.contains("description")) {
					results.getContent().get(i).setDescription(null);
				}
				if (exfields.contains("tags")) {
					results.getContent().get(i).setTags(null);
				}
				if (exfields.contains("date")) {
					results.getContent().get(i).setDate(null);
				}
				if (exfields.contains("venue")) {
					results.getContent().get(i).setVenue(null);
				}
				if (exfields.contains("prices")) {
					results.getContent().get(i).setPrices(null);
				}
				if (exfields.contains("seller")) {
					results.getContent().get(i).setSeller(null);
				}
				if (exfields.contains("photo")) {
					results.getContent().get(i).setPhoto(null);
				}
			}
		}
		return new ResponseMultipleEvents(MessagesREST.SEARCH_RESULT, results, HttpStatus.OK);
	}
	
	public ResponseSingleEvent readByCode(String code) {
		List<Event> events = this.eventRepository.findByCode(code);
		if (events.isEmpty()) {
			return new ResponseSingleEvent(MessagesREST.EVENT_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		return new ResponseSingleEvent(MessagesREST.EVENT_READ_SUCCESS, events.get(0), HttpStatus.OK);
	}
	
	public ResponseSingleEvent readByCodeSimple(String code) {
		ResponseSingleEvent responseSingleEvent = this.readByCode(code);
		Event event = responseSingleEvent.getEvent();
		if (event != null) {
			// on enlève le champ photo
			event.setPhoto(null);
			responseSingleEvent.setEvent(event);
		}
		return responseSingleEvent;
	}
	public ResponseSingleEvent readById(String id) {
		Optional<Event> event = this.eventRepository.findById(id);
		if (!event.isPresent()) {
			return new ResponseSingleEvent(MessagesREST.EVENT_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		return new ResponseSingleEvent(MessagesREST.EVENT_READ_SUCCESS, event.get(), HttpStatus.OK);
	}
	
	public ResponseSingleEvent update(Event event, JWTDecodedUser currentUser) {
		// l'objet event correspondra à ce que nous voudrons obtenir
		// comme résultat après la mise à jour
		// Champs à ne pas considérer - donc à ne pas modifier: 
		// - code
		// - seller
		// - lockRevision
		if (event.getId() == null) {
			return new ResponseSingleEvent(MessagesREST.BAD_REQUEST_ERROR, null, HttpStatus.BAD_REQUEST);
		}
		Optional<Event> lastRecord = this.eventRepository.findById(event.getId());
		if (!lastRecord.isPresent()) {
			return new ResponseSingleEvent(MessagesREST.EVENT_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		
		if (!currentUser.canUpdateEvent() && !currentUser.getCode().equals(lastRecord.get().getSeller())) {
			return new ResponseSingleEvent(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN);
		}
		
		event.setCode(lastRecord.get().getCode());
		event.setSeller(lastRecord.get().getSeller());
		event.setLockRevision(lastRecord.get().getLockRevision() + 1);
		
		// tags en lower case
		if (event.getTags() != null) {
			ArrayList<String> lowerCaseTags = new ArrayList<String>();
			for (int i = 0; i < event.getTags().size(); i++) {
				lowerCaseTags.add(event.getTags().get(i).toLowerCase());
			}
			event.setTags(lowerCaseTags);
		}
		if (!event.validity()) {
			return new ResponseSingleEvent(MessagesREST.BAD_REQUEST_ERROR, null, HttpStatus.BAD_REQUEST);
		}
		
		// gestion de nouveau prix (donc prix sans Id)
		ArrayList<Price> prices = event.getPrices();
		for (Price p : prices) {
			if (p.getId() == null) {
				p.setId(Helper.getUniqueId());
			}
		}
		
		Event savedInstance = this.eventRepository.save(event);
		
		return new ResponseSingleEvent(MessagesREST.EVENT_UPDATE_SUCCESS, savedInstance, HttpStatus.OK);
	}
}