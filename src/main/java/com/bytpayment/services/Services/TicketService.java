package com.bytpayment.services.Services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bytpayment.services.Configuration.Configuration;
import com.bytpayment.services.Helpers.Helper;
import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Helpers.TicketsPerPeriode;
import com.bytpayment.services.Model.Code;
import com.bytpayment.services.Model.Event;
import com.bytpayment.services.Model.Price;
import com.bytpayment.services.Model.QTicket;
import com.bytpayment.services.Model.Ticket;
import com.bytpayment.services.Repository.CodeRepository;
import com.bytpayment.services.Repository.EventRepository;
import com.bytpayment.services.Repository.TicketRepository;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.request.body.RequestCreateTicketByCode;
import com.bytpayment.services.request.body.RequestTicketReadAll;
import com.bytpayment.services.request.response.ResponseMultipleTickets;
import com.bytpayment.services.request.response.ResponseSingleTicket;
import com.querydsl.core.BooleanBuilder;

@Service
public class TicketService {
	@Autowired
	private TicketRepository ticketRepository;
	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private CodeRepository codeRepository;
	
	
	public ResponseSingleTicket createByCode(RequestCreateTicketByCode request, String userCode) {
		if (!request.validity()) {
			return new ResponseSingleTicket(MessagesREST.BAD_REQUEST_ERROR, null, HttpStatus.BAD_REQUEST);
		}
		
		// Récupération de l'événement
		//EventService es = new EventService(this.eventRepository);
		List<Event> events = eventRepository.findByCode(request.getEventCode());
		if (events.isEmpty()) {
			return new ResponseSingleTicket(MessagesREST.EVENT_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		Event event = events.get(0);
		
		// Récupération du code
		List<Code> codes = codeRepository.findByCode(request.getCode());
		if (codes.isEmpty()) {
			return new ResponseSingleTicket(MessagesREST.CODE_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		Code code = codes.get(0);
		
		// Si le code a été déjà utilisé, ne pas continuer
		if (code.getUsedAt() != null || code.getUsedBy() != null) {
			return new ResponseSingleTicket(MessagesREST.CODE_ALREADY_USED, null, HttpStatus.valueOf(419));
		}
		
		// TODO
		// Lecture de l'événement pour voir si il n'y aurait pas eu de changement dans l'entre-temps
		
		// génération d'un code unique - ticketNumber
		String generatedCode = "";
		do {
			generatedCode = Helper.codeGeneration(10, 20);
		} while(!this.ticketRepository.findByTicketNumber(generatedCode).isEmpty());
		
		// Test pour vérifier si on peut prendre une place
		Price price = event.priceById(request.getPriceId());
		if (price == null) {
			return new ResponseSingleTicket(MessagesREST.PRICE_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		
		if (!price.containsLocalCurrency(code.getValue())) {
			return new ResponseSingleTicket(MessagesREST.CODE_UNABLE_CREATE_TICKET, null, HttpStatus.valueOf(419));
		}
		
		Boolean canCreateTicket = false;
		if (price.getCheckExceeding() == null) {
			canCreateTicket = true;
		} else {
			if (!price.getCheckExceeding()) {
				canCreateTicket = true;
			} else {
				// vérification de la disponibilité des places
				List<Ticket> existingTickets = this.ticketRepository.findByPriceId(request.getPriceId());
				if (price.getTotalPlaces() > (price.getAlreadyTakenPlaces() + existingTickets.size())) {
					canCreateTicket = true;
				} else {
					canCreateTicket = false;
				}
			}
		}
		
		if (canCreateTicket) {
			Ticket ticket = new Ticket(
				null,
				generatedCode,
				request.getCode(),
				event.getCode(),
				event.getSeller(),
				userCode,
				request.getPriceId(),
				codes.get(0).getValue(),
				new Date(),
				true,
				null
			);
			Ticket insertedTicket = this.ticketRepository.insert(ticket);
			
			// Marquer le code comme étant utilisé
			code.setUsedBy(userCode);
			code.setUsedAt(new Date());
			this.codeRepository.save(code);
						
			return new ResponseSingleTicket(MessagesREST.TICKET_CREATION_SUCCESS, insertedTicket, HttpStatus.OK);
		}
		return new ResponseSingleTicket(MessagesREST.TICKET_CREATION_ERROR, null, HttpStatus.valueOf(419));
	}
	
	public ResponseMultipleTickets readAll(RequestTicketReadAll request) {
		QTicket qTicket = new QTicket("ticket");
		BooleanBuilder predicate = new BooleanBuilder();
		if (request.getEventCode() != null) {
			predicate.and(qTicket.eventCode.eq(request.getEventCode()));
		}
		if (request.getSeller() != null) {
			predicate.and(qTicket.seller.eq(request.getSeller()));
		}
		if (request.getUser() != null) {
			predicate.and(qTicket.user.eq(request.getUser()));
		}
		if (request.getPrice() != null) {
			if (request.getPrice().getCurrency() != null) {
				predicate.and(qTicket.price.currency.eq(request.getPrice().getCurrency()));
			}
			predicate.and(qTicket.price.value.goe(request.getPrice().getValue()));
		}
		if (request.getCreatedAt() != null) {
			predicate.and(qTicket.createdAt.goe(request.getCreatedAt()));
		}
		if (request.getValide() != null) {
			predicate.and(qTicket.valide.eq(request.getValide()));
		}
		if (request.getValideStatusChangedAt() != null) {
			predicate.and(qTicket.valideStatusChangedAt.goe(request.getValideStatusChangedAt()));
		}
		int limit = 
			request.getLimit() < 1 ? Configuration.defaultLimit : request.getLimit();
		int offset = request.getOffset(); // au lieu de null, cette valeur vaut 0
		String sortBy = request.getSortBy() == null ? Configuration.defaultTicketSortBy : request.getSortBy();
		Page<Ticket> results = (Page<Ticket>) this.ticketRepository.findAll(
			predicate,
			PageRequest.of(offset, limit, Sort.by(sortBy))	
		);
		
		return new ResponseMultipleTickets(MessagesREST.SEARCH_RESULT, results, HttpStatus.OK);
	}
	
	public ResponseSingleTicket readById(String id) {
		Optional<Ticket> ticket = this.ticketRepository.findById(id);
		if (!ticket.isPresent()) {
			return new ResponseSingleTicket(MessagesREST.TICKET_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		return new ResponseSingleTicket(MessagesREST.TICKET_READ_SUCCESS, ticket.get(), HttpStatus.OK);
	}
	
	public ResponseSingleTicket validation(String ticketNumber, JWTDecodedUser decodedUser) {
		List<Ticket> availablesTickets = this.ticketRepository.findByTicketNumber(ticketNumber);
		if (availablesTickets.isEmpty()) {
			return new ResponseSingleTicket(MessagesREST.TICKET_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		Ticket ticket = availablesTickets.get(0);
		if (!decodedUser.getCode().equals(ticket.getSeller()) && !decodedUser.canValidateTicket()) {
			return new ResponseSingleTicket(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN);
		}
		
		if (!ticket.getValide()) {
			return new ResponseSingleTicket(MessagesREST.TICKET_VALIDATED, null, HttpStatus.valueOf(419));
		}
		
		// procéder à la validation
		ticket.setValide(false);
		ticket.setValideStatusChangedAt(new Date());
		
		Ticket savedTicket = this.ticketRepository.save(ticket);
		
		return new ResponseSingleTicket(MessagesREST.TICKET_VALIDATION_SUCCESS, savedTicket, HttpStatus.OK);
	}
	
	public ResponseSingleTicket pause(String ticketNumber, JWTDecodedUser decodedUser) {
		List<Ticket> availablesTickets = this.ticketRepository.findByTicketNumber(ticketNumber);
		if (availablesTickets.isEmpty()) {
			return new ResponseSingleTicket(MessagesREST.TICKET_NOT_FOUND, null, HttpStatus.NOT_FOUND);
		}
		Ticket ticket = availablesTickets.get(0);
		if (!decodedUser.getCode().equals(ticket.getSeller()) && !decodedUser.canValidateTicket()) {
			return new ResponseSingleTicket(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN);
		}
		
		// procéder à la mise en pause
		if (!ticket.getValide() && ticket.getValideStatusChangedAt() != null) {
			ticket.setValide(true);
			ticket.setValideStatusChangedAt(new Date());
			
			Ticket savedTicket = this.ticketRepository.save(ticket);
			
			return new ResponseSingleTicket(MessagesREST.TICKET_PAUSE_SUCCESS, savedTicket, HttpStatus.OK);
		} else {
			return new ResponseSingleTicket(MessagesREST.TICKET_PAUSE_ERROR, null, HttpStatus.valueOf(419));
		}
	}
	
	public List<TicketsPerPeriode> ticketsPerWeek(String eventCode) {
		List<Ticket> tickets = this.ticketRepository.findByEventCode(eventCode);
		//Map<Integer, Integer> map = new HashMap<Integer, Integer>(); // Map<Year, weekNumber>
		Calendar calendar = Calendar.getInstance();
		
		ArrayList<TicketsPerPeriode> result = new ArrayList<TicketsPerPeriode>();
		
		for (Ticket ticket : tickets) {
			// le numéro de cette semaine + l'année
			calendar.setTime(ticket.getCreatedAt());
			int week = calendar.get(Calendar.WEEK_OF_YEAR);
			// dernier jour de cette semaine (pour bien déduire l'année)
			// par exemple pour lundi le 30/12/2019, le résultat devra être 
			// week 1 - Year 2020
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			calendar.add(Calendar.DATE, 6);
			int year = calendar.get(Calendar.YEAR);
			Boolean wnExists = false;
			
			for (TicketsPerPeriode tpp : result) {
				if (tpp.getWeek() == week) {
					wnExists = true;
					tpp.setTotalTickets(tpp.getTotalTickets() + 1);
				}
			}
			if (!wnExists) {
				TicketsPerPeriode tpp = new TicketsPerPeriode(year, week, 1);
				result.add(tpp);
			}
		}
		return result;
	}
}
