package com.bytpayment.services.Helpers;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class Helper {
	public static Boolean strEmpty(String string) {
		return (string == null || string.trim().isEmpty());
	}
	
	public static Boolean arrayListEmpty(ArrayList<String> array) {
		return (array == null || array.isEmpty());
	}
	
	public static String codeGeneration(int minSize, int maxSize) {
		int min, max;
		if (minSize == 0 || minSize >= maxSize ) {
			min = 5;
			max = 20;
		} else {
			min = minSize;
			max = maxSize;
		}
		Random random = new Random();
		int size = random.nextInt((max - min) + 1) + min;
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String code = "";
		for (int i = 0; i < size - 1; i++) {
			code += chars.charAt(random.nextInt(chars.length()));
		}
		return code;
	}
	
	public static String getUniqueId() {
		return UUID.randomUUID().toString();
	}
}
