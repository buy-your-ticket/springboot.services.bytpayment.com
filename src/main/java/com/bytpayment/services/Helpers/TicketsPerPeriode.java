package com.bytpayment.services.Helpers;

public class TicketsPerPeriode {
	private int year;
	private int week;
	private int totalTickets;
	
	public TicketsPerPeriode(int year, int week, int totalTickets) {
		this.year = year;
		this.week = week;
		this.totalTickets = totalTickets;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public int getTotalTickets() {
		return totalTickets;
	}

	public void setTotalTickets(int totalTickets) {
		this.totalTickets = totalTickets;
	}
}
