package com.bytpayment.services.Helpers;

import java.util.List;

import com.bytpayment.services.Configuration.Configuration;
import com.bytpayment.services.enums.Role;

public class JWTDecodedUser {
	private String id;
	private String code;
	private List<String> roles;
	
	public JWTDecodedUser(String id, String code, List<String> roles) {
		this.id = id;
		this.code = code;
		this.roles = roles;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean canCreateEvent() {
		if (roles == null) {
			return false;
		}
		for (Role role : Configuration.EVENT_CREATION_ROLES) {
			if (this.roles.contains(role.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public Boolean canUpdateEvent() {
		for (Role role : Configuration.EVENT_UPDATE_ROLES) {
			if (this.roles.contains(role.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public Boolean canCreateCode() {
		for (Role role : Configuration.CODE_CREATION_ROLES) {
			if (this.roles.contains(role.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public Boolean canValidateTicket() {
		for (Role role : Configuration.TICKET_VALIDATION_ROLES) {
			if (this.roles.contains(role.toString())) {
				return true;
			}
		}
		return false;
	}
}
