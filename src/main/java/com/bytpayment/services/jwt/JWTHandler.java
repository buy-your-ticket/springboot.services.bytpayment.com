package com.bytpayment.services.jwt;

import java.util.List;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bytpayment.services.Configuration.Configuration;
import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Helpers.Helper;

public class JWTHandler {
	// https://github.com/auth0/java-jwt
	public static JWTDecodedUser jwtDecode(String token) {
		try {
			Algorithm algorithm = Configuration.jwtAlgorithm;
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			String code = jwt.getIssuer();
			String id = jwt.getClaim("id").asString();
			List<String> roles = jwt.getClaim("roles").asList(String.class);
			return new JWTDecodedUser(id, code, roles);
		} catch(JWTVerificationException e) {
			return null;
		}
	}
	
	public static JWTResult getResult(String token) {
		if (token == null) {
			return new JWTResult("Authentification nécessaire pour effectuer cette opération", null);
		}
		String t = token.replaceAll("Bearer\\s*", "");
		JWTDecodedUser currentUser = JWTHandler.jwtDecode(t);
		if (currentUser == null) {
			return new JWTResult("Le token d'authentification a expiré", null);
		}
		if (Helper.strEmpty(currentUser.getCode())) {
			return new JWTResult("L'authentification a échoué, utilisateur inconnu", null);
		}
		return new JWTResult(null, currentUser);
	}
}
