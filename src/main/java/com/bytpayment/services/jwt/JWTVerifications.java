package com.bytpayment.services.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bytpayment.services.Helpers.JWTDecodedUser;
import com.bytpayment.services.Helpers.TicketsPerPeriode;
import com.bytpayment.services.constantes.MessagesREST;
import com.bytpayment.services.request.response.ResponseMessage;
import com.bytpayment.services.request.response.ResponseMultipleCodes;
import com.bytpayment.services.request.response.ResponseMultipleEvents;
import com.bytpayment.services.request.response.ResponseMultipleTickets;
import com.bytpayment.services.request.response.ResponseSingleCode;
import com.bytpayment.services.request.response.ResponseSingleEvent;
import com.bytpayment.services.request.response.ResponseSingleTicket;

public class JWTVerifications {
	public static final ResponseEntity<ResponseSingleEvent> singleEventAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		JWTDecodedUser currentUser = jwtResult.getCurrentUser();
		if (currentUser == null) {
			return new ResponseEntity<ResponseSingleEvent>(
				new ResponseSingleEvent(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (currentUser.getId() == null) {
			return new ResponseEntity<ResponseSingleEvent>(
				new ResponseSingleEvent(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseSingleEvent> singleEventNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseSingleEvent>(
				new ResponseSingleEvent(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseMultipleEvents> multipleEventsNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseMultipleEvents>(
				new ResponseMultipleEvents(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED	
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseMessage> messageResponseAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		JWTDecodedUser currentUser = jwtResult.getCurrentUser();
		if (currentUser == null) {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage(jwtResult.getMessage(), HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (currentUser.getId() == null) {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage(MessagesREST.FORBIDDEN_ERROR, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseSingleCode> singleCodeAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		JWTDecodedUser currentUser = jwtResult.getCurrentUser();
		if (currentUser == null) {
			return new ResponseEntity<ResponseSingleCode>(
				new ResponseSingleCode(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (currentUser.getId() == null) {
			return new ResponseEntity<ResponseSingleCode>(
				new ResponseSingleCode(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseSingleCode> singleCodeNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseSingleCode>(
				new ResponseSingleCode(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseMultipleCodes> multipleCodesNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseMultipleCodes>(
				new ResponseMultipleCodes(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED	
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseSingleTicket> singleTicketAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		JWTDecodedUser currentUser = jwtResult.getCurrentUser();
		if (currentUser == null) {
			return new ResponseEntity<ResponseSingleTicket>(
				new ResponseSingleTicket(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (currentUser.getId() == null) {
			return new ResponseEntity<ResponseSingleTicket>(
				new ResponseSingleTicket(MessagesREST.FORBIDDEN_ERROR, null, HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseSingleTicket> singleTicketNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseSingleTicket>(
				new ResponseSingleTicket(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
	
	public static final ResponseEntity<ResponseMultipleTickets> multipleTicketsNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<ResponseMultipleTickets>(
				new ResponseMultipleTickets(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED	
			);
		}
		return null;
	}
	
	public static final ResponseEntity<List<TicketsPerPeriode>> listTicketsPerPeriodeNoAuthorizationRequired(HttpHeaders headers) {
		JWTResult jwtResult = JWTHandler.getResult(headers.getFirst(HttpHeaders.AUTHORIZATION));
		if (jwtResult.getCurrentUser() == null) {
			return new ResponseEntity<List<TicketsPerPeriode>>(
				new ArrayList<TicketsPerPeriode>(),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
}
