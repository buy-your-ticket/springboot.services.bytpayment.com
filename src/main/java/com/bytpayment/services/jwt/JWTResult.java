package com.bytpayment.services.jwt;

import com.bytpayment.services.Helpers.JWTDecodedUser;

public class JWTResult {
	private String message;
	private JWTDecodedUser currentUser;
	
	public JWTResult(String message, JWTDecodedUser currentUser) {
		this.message = message;
		this.currentUser = currentUser;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JWTDecodedUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(JWTDecodedUser currentUser) {
		this.currentUser = currentUser;
	}	
}
