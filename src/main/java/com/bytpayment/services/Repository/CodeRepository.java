package com.bytpayment.services.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.bytpayment.services.Model.Code;

public interface CodeRepository extends MongoRepository<Code, String>, QuerydslPredicateExecutor<Code> {
	public List<Code> findByCode(String code);
	public Optional<Code> findById(String id);
}
