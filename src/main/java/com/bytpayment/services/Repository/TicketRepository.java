package com.bytpayment.services.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.bytpayment.services.Model.Ticket;

public interface TicketRepository extends MongoRepository<Ticket, String>, QuerydslPredicateExecutor<Ticket>{
	public List<Ticket> findByCode(String code);
	public List<Ticket> findByTicketNumber(String ticketNumber);
	public List<Ticket> findByPriceId(String priceId);
	public Optional<Ticket> findById(String id);
	public List<Ticket> findByEventCode(String eventCode);
}
