package com.bytpayment.services.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.bytpayment.services.Model.Event;

public interface EventRepository extends MongoRepository<Event, String>, QuerydslPredicateExecutor<Event>{
	
	public Optional<Event> findById(String id);
	
	public List<Event> findByCode(String code);
	
	public void deleteById(String id);

}
